# Initialization

1. Create react app using `npx create-react-app learnredux`

2. Install `redux` and `react-redux` using,
    ```
    cd learnredux
    npm i redux react-redux
    ```

3. Start development server to check for proper installation
    `npm run start`

# Creating Store
Create a basic store inside the **src/index.js** for now,

```
import { createStore } from 'redux';

function reducer() {
    return 'State';
}

// pass the reducer to createStore as first argument
const store = createStore(reducer)

console.log(store.getState()); // log the current state of the store
```

On checking the browser console, you will get `State` as the current state of the store
since we are returning string `State` from our simple `reducer` function.

You can dispatch an action and change the state of store by using,

```
...
const action = {
    type: 'CHANGE_STATE',
    payload: {
        newState: 'NEW STATE'
    }
};

store.dispatch(action);

// logging current state after dispatching action
console.log(store.getState());
```

You can notice that we action is just an object with two keys, `type` and `payload`
which are the generally used notation.

1. `type` -> type of action dispatched which is used to detect what will be the
response of the reducer on catching this action.

2. `payload` -> data passed from action which is used to change the current state.
(done by reducer after checking action type)

Now we have to modify our reducer function to match our action,

```
function reducer(state = null, action) {
    if (action.type === 'CHANGE_STATE') {
        return action.payload.newState
    }
    return state;
}
```

Browser console now gives us the different state value `NEW STATE`.

# Redux Devtools
You can install browser extension of Redux Devtools by googling.

Once you have installed the browser extension, you can use Redux Devtools for easily
checking the Redux state.

Modify your redux store a bit for devtools to work,

```
const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
```

This basically is checking if the browser has Redux Devtools Extension installed and if it is true then we simply call the extension using `window.__REDUX_DEVTOOLS_EXTENSION__()`

One more way to do the same thing and reducing some space is to write,

```
const store = createStore(
    reducer,
    window.devToolsExtension && window.devToolsExtension()
    // devToolsExtension is deprecated now
)
```

This is also the same thing and will work, now on checking the browser console and
switching to **Redux** tab, you can find the state of the Redux.

# Combine Multiple Reducers
Till now we have only used a simple reducer function which basically does not do
anything, what if we have multiple reducer functions which complex functionalities,
in that case we combine all the reducers using a method from `redux`,

```
import { combineReducers, createStore } from 'redux';
```

Let's also create more reducer functions,

```
function productReducer(state = [], action) {
    return state;
}

function userReducer(state = '', action) {
    return state;
}

const allReducers = combineReducers({
    products: productReducer,
    user: userReducer
});

const store = createStore(
    allReducers,
    window.devToolsExtension && window.devToolsExtension()
)
```

Now if you check the state of redux using devtools, you will find empty an object
with product as empty array and user as empty string.

Let's also pass some initial state to our store which can also be called **schema**.

```
const schema = {
    products: [{ name: 'Lenevo' }],
    user: 'Pranav'
};

const store = createStore(
    allReducers,
    schema,
    window.devToolsExtension && window.devToolsExtension()
);
```

Now our redux state has initial state of,
```
{
    products: [
        {
            name: 'Lenevo'
        }
    ],
    user: 'Pranav'
}
```

# Dispatch Action
Now let's dispatch a proper action which will update our user,

```
const updateUserAction = {
    type: 'UPDATE_USER',
    payload: {
        user: 'Alex'
    }
};

store.dispatch(updateUserAction);
```

We also need to update our `userReducer` for this action type,

```
function userReducer(state = {}, action) {
    switch(action.type) {
        case 'UPDATE_USER':
            return action.payload;
        default:
            return state
    }
}
```

Complex components can have multiple action types, in those cases we can make use of
`switch` statement in JS and accordingly return the new state.

Now on checking the redux state we get new user which is `'Alex'`.

# User Actions
Customizing action to take argument and change state dynamically, for this we will
pass argument to our action which will then set that argument in state.

Let's first define constant types by creating a separate file for this,

1. Create a file, **src/store/Types.js**.

2. Add constant for `updateUser` action,
    ```
    export const UPDATE_USER = 'users:UPDATE_USER'
    ```

    We are exporting a constant which is simply a string that will be used to define
    our action types.

    Here `'users:'` prefix is just a namespace which avoid collision when multiple
    action types exists.

Now let's import our type `UPDATE_USER` in actions and reducers,

```
import { UPDATE_USER } from '../store/Types';

export const updateUser = newUser => {
    return {
        type: UPDATE_USER,
        payload: {
            user: newUser // newUser comes from component
        }
    };
};
```

And now in our reducer,

```
import { UPDATE_USER } from '../store/Types';

export default function updateUser(state = {}, { action, payload }) {
    switch(action.payload) {
        case UPDATE_USER:
            return payload.user
        default:
            return state
    }
}
```

Now in our `App` component, let's connect our component with redux,

```
...
import { connect } from 'react-redux';

import { updateUser } from '../actions/userActions';

...

// maps state to component properties(props)
const mapStateToProps = state => ({
    user: state.user,
    products: state.products
})

// maps redux actions to component properties(props)
const mapDispatchToProps = {
    onUpdateUser: updateUser
    // onUpdateUser -> can be named anything
}

connect(mapStateToProps, mapDispatchToProps)(App)
```

Now let's use the state using props and also call actions from component props,
```
...
  onUpdateUser = e => {
    this.props.onUpdateUser('Alex');
  }

  render() {
    return (
      <div className="App">
        <h1>{this.props.user}</h1>
        <button onClick={this.onUpdateUser}>Update User</button>
      </div>
    );
  }
...
```

We can access the `updateUser` action from **src/actions/userActions.js** from our
`App` component props using, `this.props.onUpdateUser('New Name')`.

If we change `onUpdateUser` in,
```
const mapDispatchToProps = {
    changeUserName: updateUser // onUpdateUser -> changeUserName
}
```
Now we can access `updateUser` action method using `changeUserName` from component
props like this, `this.props.changeUserName('Alex');`

We also access the `user` state from props using `this.props.user`.

**NOTE: If you cannot access `this` then change `App` to class based component from
function based component.**

Checkout **src/App.js** for changing `user` state from `input` tag.

# Props & Dispatch
How can we use the components that are passed to the component itself, for example

```
function App() {
  return (
    <Main randomProp='VALUE' />
  );
}
```

For accessing the prop `randomProp` we simply need to use, `this.props.randomProp`
inside the `Main` component.

Now we will bind props to dispatch using `bindActionCreators` from `'react-redux'`.

```
import { bindActionCreators } from 'react-redux';
...
const mapDispatchToProps = dispatch => bindActionCreators({
    onUpdateUser: updateUser
}, dispatch);
```
This way we bind the **actions** with **dispatch**.

One more argument that `connect` takes is `mergeProps`.

We can merge the props using this third argument passed to `connect`.

```
const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
    return {};
}
```

# Redux Thunk
Install `redux-thunk` and `jquery` using,
`npm i redux-thunk jquery`

We need Thunk middleware in our application because we cannot always return a object
from our actions, since till now our actions have been just simple functions returning
a object with **type** and **payload**. In complex projects this is not always the case,
we will need to return a function from our action, we can do this with Thunk middleware.

To implement Thunk middleware,

```
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

const allStoreEnhancers = compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(
    schema,
    allStoreEnhancers
);
```

Now we have applied Thunk middleware to our redux store.

# Advanced Actions
Now we can have actions which can return a function and a lot of other things and not
just a object with the help of Redux Thunk middleware.

```
import $ from 'jquery';
...
export function apiRequest() {
    return dispatch => {
        $.ajax({
            url: 'https://google.com',
            success: res => {
                console.log(res);
            },
            error: err => {
                console.error(err);
            }
        })
    }
}
```

We are using Jquery for making an API request, to the google.com domain for
demonstration purposes and log the responses to the console.

Now we are going to call this action from our `Main` component.

```
import { apiRequest, updateUser } from '../actions/userActions';
...
const mapDispatchToProps = {
    onUpdateUser: updateUser,
    onApiRequest: apiRequest
};
```

Don't forget to map our action to component props.

Now call this action from `componentDidMount`,

```
...
class Main extends Component {
  ...
  componentDidMount() {
    this.props.onRequestAPI();
  }
  ...
...
```

Now if we check the console, we can see the error from the API request.

### Dispatch Action from Action
Since we are returning `dispatch` function from `apiRequest` action, we can dispatch
other actions using `dispatch`.

Using `dispatch` we will call an action to display error if API request fails.

```
import { SHOW_ERROR, UPDATE_USER } from '../store/Types';
...
export const showError = error => {
    return {
        type: SHOW_ERROR,
        payload: {
            message: error
        }
    }
};
```

Now inside our `apiRequest` function, we can dispatch `showError` action on API request
failure,

```
export const APIRequest = () => {
  return dispatch => $.ajax({
    url: 'https://jsonplaceholder.typicode.com/posts/',
    success: res => {
      console.log(res);
    },
    error: err => {
      dispatch(showError(err.statusText));
    }
  })
}
```

Now in our `userReducer` we need to another switch case for this new action type,

```
...
  switch(type) {
    case UPDATE_USER:
      return payload.user
    case SHOW_ERROR:
      return payload.message
    default:
      return state
  }
...
```

And on refreshing the browser, we can see that the error message appears instead
of the value stored in the state.

# [Reselect](https://github.com/reduxjs/reselect)
Checkout [Reselect](https://github.com/reduxjs/reselect) library for Redux which
helps us in many [ways](https://github.com/reduxjs/reselect#reselect).

Install using, `npm i reselect`.

Inside **components/Main.js**,
```
import { createSelector } from 'reselect';

const mapStateToProps = createSelector(
    state => state.user,
    state => state.products,
    (products, user) => ({
        products,
        user
    })
)
```

`createSelector` takes arguments which are functions which are passed to state,

It is good to create smaller selectors, which increases speed and are not recomputed
on changes to state,

```
const userSelector = createSelector(
    state => state.user,
    user => user
);

const productSelector = createSelector(
    state => state.products,
    products => products
);

const mapStateToProps = createSelector(
    userSelector,
    productSelector,
    (user, products) => ({
        products,
        user
    })
)
```
