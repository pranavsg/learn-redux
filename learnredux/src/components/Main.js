import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { updateUser, APIRequest } from '../actions/userActions';

class Main extends Component {
  onUpdateUser = e => {
    this.props.onUpdateUser(e.target.value)
  }

  componentDidMount() {
    // this.props.onRequestAPI();
  }

  render() {
    // console.log(this.props); // access component props

    return (
      <div className="App">
        <h1>{this.props.user}</h1>
        <input
          name='username'
          value={this.props.user}
          onChange={this.onUpdateUser}
        />
      </div>
    );
  }
}

const productSelector = createSelector(
  state => state.products,
  products => products
);

const userSelector = createSelector(
  state => state.user,
  user => user
);

const mapStateToProps = createSelector(
  productSelector,
  userSelector,
  (products, user) => ({
    products,
    user
  })
)

const mapDispatchToProps = {
  onUpdateUser: updateUser,
  onRequestAPI: APIRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
