import $ from 'jquery';

import { UPDATE_USER, SHOW_ERROR } from '../store/Types';

export const updateUser = newUser => {
  return {
    type: UPDATE_USER,
    payload: {
      user: newUser
    }
  }
};

export const showError = error => {
  return {
    type: SHOW_ERROR,
    payload: {
      message: error
    }
  }
}

export const APIRequest = () => {
  return dispatch => $.ajax({
    url: 'https://jsonplaceholder.typicode.com/postsff/',
    success: res => {
      console.log(res);
    },
    error: err => {
      dispatch(showError(err.statusText));
    }
  })
}
