import { UPDATE_USER, SHOW_ERROR } from '../store/Types';

export default function userReducer(state = {}, { type, payload }) {
  switch(type) {
    case UPDATE_USER:
      return payload.user
    case SHOW_ERROR:
      return payload.message
    default:
      return state
  }
}
