import { combineReducers } from 'redux';

import productsReducer from './productsReducer';
import userReducer from './userReducer';

// combining multiple reducers and passing them to createStore
const allReducers = combineReducers({
  products: productsReducer,
  user: userReducer
});

export default allReducers;
