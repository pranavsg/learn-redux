export const UPDATE_USER = 'users:UPDATE_USER';
// users -> avoid collisions with other types

export const SHOW_ERROR = 'users:SHOW_ERROR';
